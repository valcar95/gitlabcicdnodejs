var express = require('express');
var serveIndex = require('serve-index');
var app = express();

app.use('/static', express.static('archivos'));

app.use(express.static('archivos' + "/"))
app.use('/static', serveIndex('archivos',{'icons': true, 'view':'details'}));

app.listen(3000, function () {});